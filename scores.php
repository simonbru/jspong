<?php
//Importation de la configuration de la BDD
include 'config.php';

//Les tableaux qui seront envoyés en JSON à la fin de la page:
$errors = array();
$rank = array();

//fonction qui s'occupera de renvoyer les résultats en JSON
function printJSON() {
	$data = array(
		"errors" => $GLOBALS['errors'],
		"rank" => $GLOBALS['rank'],
	);
	echo json_encode($data);
}

//fonction qui gère les exceptions hors des blocs try ... catch
function exception_handler($e) {
	$errors[] = $e->getMessage();
	printJSON();
	exit();
}
set_exception_handler('exception_handler');

//Connection à la base de données
$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
$bdd = new PDO('mysql:host='.$CONFIG['sql_host'].';dbname='.$CONFIG['sql_db'], 
				$CONFIG['sql_username'], $CONFIG['sql_password'], $pdo_options);

//On récupère l'action demandée par le client
if(!empty($_POST['query']))
	$query = $_POST['query'];
else
	$query = false;
	
//On effectue l'action demandée
switch ($query) {
	case "sendScore":
		//On vérifie les infos reçues
		if (empty($_POST['nameWinner']) || empty($_POST['nameLoser']) || empty($_POST['score'])) {
			$errors[] = "Il manque des informations !";
			break;
		}
		$nameWinner = $_POST['nameWinner'];
		$nameLoser = $_POST['nameLoser'];
		$score = $_POST['score'];
		if (count($nameWinner) > 20 || count($nameLoser) > 20) {
			$errors[] = "Un des noms est trop long !";
			break;
		}
		if ($score < 1 || $score > 10) {
			$errors[] = "Le score est invalide !";
			break;
		}
		
		//On vérifie l'existence du gagnant et du perdant dans la bdd
		//S'ils n'existent pas, on leur crée une entrée
		$querySelect = $bdd->prepare("SELECT idPlayer FROM players WHERE namePlayer = ?");
		$queryInsert = $bdd->prepare("INSERT INTO players (namePlayer) VALUES (?)");
		
		//On le fait d'abord pour le gagnant
		$querySelect->execute(array($nameWinner));
		$idWinner = $querySelect->fetchColumn();
		if (!$idWinner) {
			$queryInsert->execute(array($nameWinner));
			$idWinner = $bdd->lastInsertId();
		}
		//puis pour le perdant.
		$querySelect->execute(array($nameLoser));
		$idLoser = $querySelect->fetchColumn();
		
		if (!$idLoser) {
			$queryInsert->execute(array($nameLoser));
			$idLoser = $bdd->lastInsertId();
		}
		
		//On sauvegarde le score dans la base de données
		$queryInsert = $bdd->prepare("INSERT INTO scores VALUES (NULL, :idPlayer, :score, NOW())");
		
		$queryInsert->execute(array(
			'idPlayer' => $idWinner,
			'score' => $score
			));
		$queryInsert->execute(array(
			'idPlayer' => $idLoser,
			'score' => 0
		));
		break;
		
	//Récupère les meilleurs scores
	case "getScores":
		$query = $bdd->query("
			SELECT p.namePlayer, SUM(score) scoreTotal, COUNT(idScore) nbGames, MAX(datetime) lastGame
			FROM scores s 
			INNER JOIN players p ON p.idPlayer = s.idPlayer 
			GROUP BY p.idPlayer ORDER BY scoreTotal DESC LIMIT 0, 12");
		//On stocke tout dans un tableau à 2 dimensions
		$rank = $query->fetchAll(PDO::FETCH_ASSOC);
		break;
	
	//Si query ne correspond à aucune action on avertit le client
	default:
		$errors[] = "Requête inconnue !";
}


//On envoie le résultat
printJSON();
