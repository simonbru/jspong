//Objet global qui fait office de namespace
function JSPong() {
	//Initialisation des attributs

	//Tableau associatif (ou objet) qui référencie les pages
	this.pages = [];
	var pagesTemp = document.getElementsByClassName('divPage');
	for (i in pagesTemp) {
		this.pages[pagesTemp[i].id] = pagesTemp[i]
	}
	this.currentPage;
	
	//Le bloc principal qui contient chaque page
	this.mainDiv = document.getElementById('mainDiv');

	
	//Références vers les champs/options
	this.showFirstHelp = false; //Affiche l'aide à la première partie
	//Note : Option désactivée pour le moment car plus désagréable qu'utile
	
	//Initialisation de la page
	//On peut charger une page précise via le hash
	if (window.location.hash)
		this.changePage(window.location.hash.substr(1));
	else
		this.changePage('pageWelcomeScreen');
		
	//RegisterKeys 
	this.pressedKeys = {};
	
	//Référence vers l'objet Game
	this.game;
}

JSPong.prototype.changePage = function(idPage) {
	//Si la page idPage existe, on remplace le contenu de mainDiv par celle-là
	if (this.pages[idPage]) {
		this.mainDiv.innerHTML = '';
		this.mainDiv.appendChild(this.pages[idPage])
		this.pages[idPage].style.display = 'block';
		
		//On actionne la méthode qui pourrait être associée au chargement de la page
		if (this['onShow_'+idPage]) {
			this['onShow_'+idPage]();
		}
		this.currentPage = idPage;
	}
	else
		throw "Page "+idPage+" introuvable";
	
}

JSPong.prototype.createGame = function() {
	var namePlayer1 = document.getElementById('namePlayer1').value;
	var namePlayer2 = document.getElementById('namePlayer2').value;
	
	//On vérifie rapidement les noms
	if (namePlayer1 != '' && namePlayer1 == namePlayer2) {
		alert("Vous avez choisis 2 noms identiques !");
		return;
	}
	else if (namePlayer1.length > 20 || namePlayer2.length > 20) {
		alert("Les noms ne doivent pas dépasser 20 caractères !");
		return;
	}
	
	//S'il manque un ou 2 noms, on désactive le classement pour la partie
	var noRank = false;
	if (namePlayer1 == '' || namePlayer2 == '')
		noRank = true;
		
	
	//On s'assure que l'ancienne partie est supprimée avant d'en refaire une nouvelle
	if (this.game) {
		this.game.destroy();
	}
	this.game = true; //On le définit à vrai pour que onShow_pageHelp nous ramène dans le jeu au lieu du menu
	this.changePage('pageGame');
	this.game = new Game(namePlayer1, namePlayer2, noRank);
}

//Fonction qui envoie le score en fin de partie
JSPong.prototype.sendScore = function(winner, callback) {
	//Données transmises en POST
	var data = {
		query: 'sendScore',
		nameWinner: winner.name,
		score: winner.score
	}
	if (winner.id == 1)
		data.nameLoser = this.game.player2.name;
	else if (winner.id == 2)
		data.nameLoser = this.game.player1.name;
		
	$.ajax({
		//Paramètres de la requête
		url: "scores.php",
		data: data,
		dataType: 'json',
		type: 'post',
		success: function(dataGot) {
			//Si la requête s'est correctement déroulée, on passe les données reçues à la fonction de callback
			callback(dataGot);
		},
		error: function(jqXHR, textStatus, errorThrown){
			//La requête n'a pas abouti, on en note la raison
			var errors = new Array(); //Contient les erreurs rencontrées en local
			if (textStatus == 'timeout') {
				errors.push('Le serveur met trop de temps à répondre');
			}
			else if(textStatus == 'abort') {
				errors.push('La connexion a été annulée');
			}
			else if(textStatus == 'error') {
				errors.push(errorThrown);
			}
			else
				errors.push(textStatus);
			
			var data = {
				errors: errors
			};
			callback(data);
		},
	});
}

JSPong.prototype.showScores = function() {
	//Données transmises en POST
	$.ajax({
		url: "scores.php",
		data: {query: 'getScores'},
		dataType: 'json',
		type: 'post',
		success: function(dataGot) {
			//Si la requête s'est correctement déroulée, on affiche les éventuelles erreurs envoyées par le serveur
			document.getElementById('rankErrors').innerHTML = '';
			if (dataGot.errors.length) {
				for (i in dataGot.errors)
					document.getElementById('rankErrors').innerHTML += dataGot.errors[i];
			}
			
			//Sinon on affiche les données
			else {
				window.dataGot = dataGot;
				var table = document.getElementById('tableScores');
				var tr;
				var td;
				//on vide l'ancien tableau
				var tHead = table.getElementsByClassName('tableHead')[0];
				table.innerHTML = '';
				table.appendChild(tHead);
				
				//On parcourt dataGot afin de l'afficher dans un tableau HTML
				for (i in dataGot.rank) {
					tr = document.createElement('tr');
					for (j in dataGot.rank[i]) {
						td = document.createElement('td');
						td.appendChild(document.createTextNode(dataGot.rank[i][j]));
						tr.appendChild(td);
					}
					table.appendChild(tr);
				}
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			var error; 
			//La requête n'a pas abouti, on en note la raison
			if (textStatus == 'timeout') {
				error = 'Le serveur met trop de temps à répondre';
			}
			else if(textStatus == 'abort') {
				error = 'La connexion a été annulée';
			}
			else if(textStatus == 'error') {
				error = errorThrown;
			}
			else
				error = textStatu;
			//Puis on l'affiche dans la page
			document.getElementById('rankErrors').innerHTML  = error;
		},
		complete: function() {
			
		}
	});
}

//Méthodes appelées lors de l'affichage de certaines pages
JSPong.prototype.onShow_pageGame = function() {
	//Si c'est la première partie, on affiche l'aide
	if (this.showFirstHelp == true) {
		this.showFirstHelp = false;
		this.changePage('pageHelp');
	}
}

JSPong.prototype.onShow_pageHelp = function() {
	var span = this.pages.pageHelp.getElementsByClassName('navBar')[0].getElementsByTagName('span')[0];
	if (this.game) {
		span.innerHTML = "Retour au jeu";
		span.onclick = function() {JSPong.changePage('pageGame')};
	}
	else {
		span.innerHTML = "Revenir au menu";
		span.onclick = function() {JSPong.changePage('pageMainMenu')};
	}
}

JSPong.prototype.onShow_pageRank = function() {
	this.showScores();
}

//Méthodes enregistrant les touches appuyées pour le jeu
JSPong.prototype.registerKeys = function(event, value) {
	//On ajoute la lettre de la touche en tant qu'attribut de pressedKeys. On lui associe la valeur true lorsqu'elle est appuyée et on la supprime lorsqu'elle est relâchée
	var key = String.fromCharCode(event.keyCode).toLowerCase();
	if (value) {
		this.pressedKeys[key] = true;
	}
	else
		delete this.pressedKeys[key];
		
	//S'il s'agit d'un raccourci clavier simple (pas besoin de fluidité), on le traite ici directement
	if (value) {
		switch(key) {
			case " ": //[Space]
				if(this.currentPage == 'pageGame') {
					document.getElementById('togglePause').onclick();
				}
				break;
			case "q":
				if(this.currentPage == 'pageGame') {
					document.getElementById('quitLink').onclick();
				}
				break;
		}
	}
};
document.onkeydown = function(event) {JSPong.registerKeys(event, true)};
document.onkeyup = function(event) {JSPong.registerKeys(event, false)};