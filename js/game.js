//Objet Game, créé à chaque lancement de partie
function Game(pNamePlayer1, pNamePlayer2, noRank) {
	//Initialisation des attributs
	//(On en profite pour actualiser les champs correspondant sur l'interface du jeu)
	this.player1 = {
		id: 1,
		score: 10
	};
	this.player1.name = (pNamePlayer1 != "") ? pNamePlayer1 : "Joueur 1";
	document.querySelector('#pPlayer1 .playerName').innerHTML = this.player1.name;
	document.querySelector('#pPlayer1 .score').innerHTML = this.player1.score;
	
	this.player2 = {
		id: 2,
		score: 10
	};
	this.player2.name = (pNamePlayer2 != "") ? pNamePlayer2 : "Joueur 2";
	document.querySelector('#pPlayer2 .playerName').innerHTML = this.player2.name;
	document.querySelector('#pPlayer2 .score').innerHTML = this.player2.score;

	if (noRank)
		this.noRank = true;
	
	//Paramètres de la balle
	this.ballSpeedX = 6;
	this.maxSpeedX = 20;
	this.ballSpeedY = 0;
	this.firstShot = 1; //Définit à qui est l'engagement
	this.tooLate = false; //Plus d'explications plus bas
	this.waitInterval = false;
	
	//Initialisation des items
	this.racket1 = new Item(10,165, 'res/raquette.png', 'racket');
	this.racket2 = new Item(675,165, 'res/raquette.png', 'racket');
	this.ball = new Item(343,200, 'res/balle.png', 'ball');
	
	//On démarre la partie
	this.state = 'pause';
	this.changeState('continue');
		
	//Initialisation des évènements
	var $this = this; //Astuce pour éviter que this pointe vers autre chose que notre objet Game
	document.getElementById('togglePause').onclick = function(){$this.changeState('pause')};
}

//On implémente une sorte de destructeur qui supprime les éléments de la page lorsque c'est nécessaire
Game.prototype.destroy = function() {
	clearInterval(this.loopInterval);
	clearInterval(this.waitInterval);
	var gameSpace = document.getElementById('gameSpace');
	gameSpace.removeChild(this.racket1.elem);
	gameSpace.removeChild(this.racket2.elem);
	gameSpace.removeChild(this.ball.elem);
	document.getElementById('gameMessage').display = 'none';
	document.getElementById('sendScore').display = 'none';
	delete JSPong.game;
}

//Boucle principale du jeu
Game.prototype.loop = function() {
	//On déplace les raquettes
	this.moveRackets();
	
	//On déplace la balle et on récupère le numéro de l'éventuel gagnant
	//On s'occupe de compter les points et on relance la balle
	var winner = this.moveBall();
	if (winner) {
		if (winner.id == 1) {
			this.player2.score--;
			document.querySelector('#pPlayer2 .score').innerHTML = this.player2.score;
		}
		else if (winner.id == 2) {
			this.player1.score--;
			document.querySelector('#pPlayer1 .score').innerHTML = this.player1.score;
		}
		
		if (this.player1.score == 0 || this.player2.score == 0) {
			this.changeState('endGame', {'winner': winner});
		}
		else {
			//On replace la balle et on relance la partie
			//On réinitialise certains attributs
			this.changeState('pause');
			this.firstShot *= -1;
			this.ballSpeedX = 6*this.firstShot;
			this.ballSpeedY = 0;
			this.ball.setPosX(343, true);
			this.ball.setPosY(200, true);
			this.racket1.setPosY(165, true);
			this.racket2.setPosY(165, true);
			this.tooLate = false;
			this.changeState('continue');
		}
	}
}

Game.prototype.moveRackets = function() {
	//Mouvement de la raquette gauche
	var gap = 9;
	if (JSPong.pressedKeys['w']) {
		this.racket1.setPosY(-gap);
	}
	if (JSPong.pressedKeys['s']) {
		this.racket1.setPosY(gap);
	}
	
	//Mouvement de la raquette droite
	if (JSPong.pressedKeys['i']) {
		this.racket2.setPosY(-gap);
	}
	if (JSPong.pressedKeys['k']) {
		this.racket2.setPosY(gap);
	}
}

Game.prototype.moveBall = function() {
	//speedAccel permet de s'assurer qu'on ne dépasse pas la vitesse maximale définit par maxSpeedX
	var speedAccel = 1;
	if (this.maxSpeedX < this.ballSpeedX) //Petit contrôle
		speedAccel = 0;
	
	
	//Tout d'abord on déplace la balle
	var wallHitX = this.ball.setPosX(this.ballSpeedX);
	var wallHitY = this.ball.setPosY(this.ballSpeedY);
	
	//Si la balle a touché un mur horizontal, on inverse la vitesse vertical pour qu'elle rebondisse
	if (wallHitY)
		this.ballSpeedY *= -1;
	
	//On contrôle si la balle doit rebondir contre la raquette.
	//Si celle-ci passe derrière une raquette, on définit tooLate comme vrai, afin d'éviter des rebonds 
	
	//Si la balle avance vers le mur et dépasse la limite horizontale de rebond de la raquette
	if (this.ballSpeedX<0 &&
		this.ball.posX < (this.racket1.posX + this.racket1.sprite.width)){
			
		//Si la balle se trouve derrière la raquette à la frame précise où elle dépasse la limite horizontale (on le sait grâce à tooLate)
		//remarque : sans l'attribut tooLate, le joueur pourrait "remettre la balle en jeu" en plaçant sa raquette au dessus de la balle avant qu'elle touche le mur.
		if (this.ball.posY > this.racket1.posY-this.ball.sprite.height && 
			this.ball.posY < this.racket1.posY+this.racket1.sprite.height && 
			!this.tooLate) {
			//On considère que la balle doit rebondir sur la raquette, en gagnant de la vitesse (on prend en compte le signe de la vitesse)
			//Plus la balle rebondit au bord de la raquette, plus elle sera déviée et accélérée
			var coef = this.racket1.posY + (this.racket1.sprite.height/2) - (this.ball.posY+this.ball.sprite.height);
			this.ballSpeedX -= speedAccel*(Math.abs(coef)-15)/40;
			this.ballSpeedY = (this.ballSpeedX*coef)/(70);
			this.ballSpeedX *= -1;
		}
		//Sinon on laisse la balle avancer vers le mur, et on définit tooLate comme vrai car on sait déjà que la partie sera finie
		else 
			this.tooLate = true;
	}
	
	//On refait la même procédure qu'au dessus mais pour la raquette de droite
	else if (this.ballSpeedX>0 &&
		this.ball.posX + this.ball.sprite.width > this.racket2.posX) {
			
		if (this.ball.posY > this.racket2.posY-this.ball.sprite.height && 
			this.ball.posY < this.racket2.posY+this.racket2.sprite.height && 
			!this.tooLate) {
			//On considère que la balle doit rebondir sur la raquette, en gagnant de la vitesse
			var coef = this.racket2.posY + (this.racket2.sprite.height/2) - (this.ball.posY+this.ball.sprite.height);
			this.ballSpeedX += speedAccel*(Math.abs(coef)-15)/40;
			this.ballSpeedY = (this.ballSpeedX*coef)/(-70);
			this.ballSpeedX *= -1;
		}
		//Sinon on laisse la balle avancer vers le mur, et on définit tooLate comme vrai car on sait déjà que la partie sera finie
		else 
			this.tooLate = true;
	}

	//On vérifie si la balle a touché un mur vertical grâce à wallHitX. 
	//Si c'est le cas, on retourne le gagnant, autrement on retourne faux
	if (wallHitX && this.tooLate) {
		if (this.ballSpeedX > 0)
			return this.player1;
		else
			return this.player2;
	}
	else
		return false;
}

//Gère les différents états du jeu (pause, en cours, fin de partie, ...)
//map est un objet contenant divers attributs selon le besoin
Game.prototype.changeState = function(stateName, map) {
	var gameMessage = document.getElementById('gameMessage');
	var sendScore = document.getElementById('sendScore');
	var togglePause = document.getElementById('togglePause');
	var $this = this;
	
	var newState = stateName;
	
	switch (stateName) {
		//Fin de la partie, envoi des scores
		case 'endGame':
			clearInterval(this.loopInterval);
			gameMessage.innerHTML = map.winner.name+" a remporté la partie !";
			gameMessage.style.display = 'block';
			if (!this.noRank) {
				sendScore.style.display = 'block';
				sendScore.innerHTML = "Sauvegarde du score en cours...";
				//On envoie les scores et on exécute une fonction à la fin de la requête
				JSPong.sendScore(map.winner, function(data) {
					if (data) {
						if (!data.errors.length) {
							sendScore.innerHTML = 'Le score a été envoyé avec succès !<br /><a href="#" onclick="JSPong.changePage(\'pageRank\');return false;">Voir le classement</a>';
						}
						else {
							//On affiche les erreurs s'il y en a
							sendScore.innerHTML = '';
							for (i in data.errors) {
								sendScore.innerHTML += "<p>"+data.errors[i]+"</p>";
							}
						}
					}
				});
			}
			togglePause.innerHTML = 'Recommencer';
			togglePause.onclick = function(){$this.changeState('restart')};
			break;
			
		case 'pause':
			if (this.state != 'continue')
				return;
			//On stoppe la boucle, on affiche le mode pause
			clearInterval(this.loopInterval);
			togglePause.innerHTML = 'Continuer [Space]';
			togglePause.onclick = function(){$this.changeState('continue')};
			gameMessage.innerHTML = "Jeu en Pause";
			gameMessage.style.display = 'block';
			break;
			
		case 'continue':
			if (this.state != 'pause')
				return;
			//togglePause.innerHTML = '';
			//togglePause.onclick = false;
			var count = 3;
			var $this = this; //Astuce pour éviter que this pointe vers autre chose que notre objet Game
			gameMessage.innerHTML = count;
			//On lance un compte à rebours à l'écran afin que les joueurs puissent se préparer à jouer
			//On place le jeu dans un état spécial afin que l'on ne puisse pas faire pause/continuer pendant le temps d'attente
			newState = 'wait';
			this.waitInterval = setInterval(function() {
				if (count > 1) {
					count--;
					gameMessage.innerHTML = count;
				}
				else {
					togglePause.innerHTML = 'Pause [Space]';
					togglePause.onclick = function(){$this.changeState('pause')};
					gameMessage.style.display = 'none';
					$this.state = 'continue';
					$this.loopInterval = setInterval(function(){$this.loop()}, 20);
					clearInterval($this.waitInterval);
				}
			}, 700);
			
			break;
		
		case 'restart':
			//On détruit la partie et on en crée une autre
			this.destroy();
			JSPong.game = new Game(this.player1.name, this.player2.name);
		
		//Retour au menu
		case 'quit':
			this.destroy();
			JSPong.changePage('pageMainMenu');
			break;
		
		//Si on demande un état qui existe pas, l'état n'a pas changé
		default:
			newState = this.state;
	}
	//On met à jour l'état du jeu
	this.state = newState;
}


//Objet item, correspondant à chaque item du jeu
function Item(posX, posY, spriteUrl, className) {
	//Position de l'item dans l'espace de jeu
	this.posX = posX;
	this.posY = posY;
	
	//Div de l'item
	this.elem = document.createElement('div');
	this.elem.className = 'item '+className;
	this.elem.style.top = posY+'px';
	this.elem.style.left = posX+'px';
	
	//Img de l'item
	this.sprite = document.createElement('img');
	this.sprite.onload = function() {
	}
	this.sprite.src = spriteUrl;
	this.sprite.style.display = 'block';
	this.elem.appendChild(this.sprite);
	document.getElementById('gameSpace').appendChild(this.elem);
}

//Setteurs de Item
Item.prototype.setPosX = function(gap, isAbsolute) {
	//Si isAbsolute est vrai, alors on applique la nouvelle position sans rien contrôler
	if (isAbsolute) {
		this.posX = gap;
		wallHit = false; 
	}
	
	else {
		var leftLimit = 0;
		var rightLimit = 700;
		
		//On fait en sorte que les item ne sortent pas du cadre.
		//On définit wallHit à vrai si la balle touche un mur vertical
		var wallHit = false;
		if (this.posX + gap < leftLimit) {
			this.posX = leftLimit;
			wallHit = true;
		}
		else if(this.posX + this.sprite.width + gap > rightLimit) {
			this.posX = (rightLimit - this.sprite.width);
			wallHit = true;
		}
		else {
			this.posX += gap;
		}
	}
	
	this.elem.style.left = this.posX + 'px';
	return wallHit;
}

Item.prototype.setPosY = function(gap, isAbsolute) {
	//Si isAbsolute est vrai, alors on applique la nouvelle position sans rien contrôler
	if (isAbsolute) {
		this.posY = gap;
		wallHit = false; 
	}
	else {
		//On fait en sorte que les item ne sortent pas du cadre.
		var wallHit = false;
		
		if (this.posY + gap < 0) {
			this.posY = 0;
			wallHit = true;
		}
		else if(this.posY + this.sprite.height + gap > 420) {
			this.posY = (420 - this.sprite.height);
			wallHit = true;
		}
		else {
			this.posY += gap;
		}
	}
	
	this.elem.style.top = this.posY + 'px';
	return wallHit;
}